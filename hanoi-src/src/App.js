import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const titlePage = 'Torres de Hanoi - React JS';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p className="title-page">{titlePage}</p>
        </header>
        <Components></Components>
        <Towers></Towers>
      </div>
    );
  }
}
class Components extends Component{
  render(){
    return(
      <div className="search-content">
        <div>
          <input className="input-cont" placeholder="Insert total disc" type="number"></input>
          <button className="btn-play">Play</button>
          <button className="btn-restar">Restart</button>
        </div>
      </div>
    );
  }
}

class Towers extends Component{
  render(){
    return(
      <div>
        <div className="tower-content tw-1">
          <div className="horizontal-line"></div>
          <div className="vertical-line"></div>
        </div>
        <div className="tower-content tw-2">
          <div className="horizontal-line"></div>
          <div className="vertical-line"></div></div>
        <div className="tower-content tw-3">
          <div className="horizontal-line"></div>
          <div className="vertical-line"></div>
        </div>
        <Discs></Discs>
      </div>
    );
  }
}

class Discs extends Component{
  render(){
    return(
      <div>
        <div id="disk1" className="disk"></div>
        <div id="disk2" className="disk"></div>
        <div id="disk3" className="disk"></div>
        <div id="disk4" className="disk"></div>
        <div id="disk5" className="disk"></div>
      </div>
    );
  }
}

export default App;

